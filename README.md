# Component Monitor Agent

This is the client (agent) for the Component Monitor application. It runs as a background task on a customer server, monitoring client infrastructure and sending status updates to a cloud-based REST API.


## Access

Components can be accessed as follows:

1. The [browser interface][cmui] provides a graphical display of system components (requires a Javascript-enabled browser)
2. The [API][cmapi] provides a programmatic interface (for REST clients)

[cmapi]: https://compmon.dynu.net/api/1.0/
[cmui]:  https://compmon.dynu.net/


## Configuration

Python 3.6 or later must be installed locally, also virtualenv and all pip packages from the requirements.txt

A customer-specific authentication token is required for the REST API, this should be downloaded from the Administration site and placed in a file 'cmtoken.txt' in the same folder as the application zipfile.

Local server details, service configuration settings, and credentials are stored in the encyrypted INI file 'cmagent.inx', this file should be generated locally in conjunction with support staff


## Startup

The client is normally started from a Windows Scheduled Task, scheduled to run at regular intervals throughout the day. If the process detects an existing agent is already running it will attempt to kill it and take over processing

The client can be launched manually from the command line:

    CD C:\cmagent
    .\env\Scripts\python.exe cmagent


## Changes

Initial release




